import React, { Component } from 'react'

import "./style.css";
import "./Quizbank";
import Quizbank from './Quizbank';
import Questionbox from './Components/Questionbox';
import Result from './Components/Result';
export default class Quiz extends Component {

  state ={
    questionBank:[],
    score:0,
    responses:0
  };

  getQuestions = ()=>{
    Quizbank().then(question =>{
      this.setState({
        questionBank:question
      })
    })
  };

  computeAnswer=(answer,correctAnswer)=>{
    if(answer === correctAnswer){
      this.setState({score:this.state.score + 1})
    }
    this.setState({responses:this.state.responses <5 ? this.state.responses +1 :5})
  }

  playAgain =()=>{
    this.getQuestions();
    this.setState({
      score:0,responses:0
    })
  }
  componentDidMount(){
    this.getQuestions();
  }

  render() {
    return (
      <div className=''>
        <div className='title'>QuizGame</div>
        {this.state.questionBank.length >0 && this.state.responses < 5 && this.state.questionBank.map(({question,answers,correct,questionId})=><Questionbox question={question} options={answers} 
        key={questionId}
        selected={answer =>this.computeAnswer(answer,correct)}></Questionbox>)}
        {this.state.responses === 5 ? <Result score={this.state.score} playAgain={this.playAgain}/> : null}
      </div>
    )
  }
}
